package modules.android.app.roomsampledatabase;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;

public class WordRepository {

   private WordDAO mWordDao;
   private LiveData<List<Word>> mAllWords;

   WordRepository(Application application) {
       RoomDatabase db = RoomDatabase.getDatabase(application);
       mWordDao = db.wordDAO();
       mAllWords = mWordDao.getList();
   }

   LiveData<List<Word>> getAllWords() {
       return mAllWords;
   }


   public void insert (Word word) {
       new insertAsyncTask(mWordDao).execute(word);
   }

   public void delete()
   {
       new deleteTask().execute();
   }

   private static class insertAsyncTask extends AsyncTask<Word, Void, Void> {

       private WordDAO mAsyncTaskDao;

       insertAsyncTask(WordDAO dao) {
           mAsyncTaskDao = dao;
       }

       @Override
       protected Void doInBackground(final Word... params) {
           mAsyncTaskDao.insert(params[0]);
           return null;
       }
   }

    private class deleteTask extends AsyncTask<Void,Void,Void>{
        @Override
        protected Void doInBackground(Void... voids) {
            mWordDao.deleteAll();

            return null;
        }


    }
}