package modules.android.app.roomsampledatabase;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "MyTable")
public class Word {

    @PrimaryKey(autoGenerate = true)
    int id;

    @NonNull
    @ColumnInfo(name = "Words")
    String word;

    public Word(String word) {
        this.word = word;
    }

    public String getWord() {
        return word;
    }

}
